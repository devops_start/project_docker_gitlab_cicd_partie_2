# Exercice 1 : Dockerisation d'une Application Web

## Projet de Site Web Statique Simple

- **But**: L'objectif de cet exercice est de créer une application web statique simple à l'aide d'HTML et de CSS. Cette application web est destinée à être utilisée localement avec l'aide de Docker et Jekyll. Nous allons également utiliser le serveur Webrick pour afficher le site.

## Etape 1 : Clonez ce repository GitHub sur votre système :
```
git clone https://gitlab.com/devops_start/project_docker_gitlab_cicd.git
```

## Étape 2 : Construire l'Image Docker
- Dans le terminal, accédez au répertoire de votre Dockerfile, qui est */exo_1*.
- Exécutez la commande
 ```
sudo docker build -t monsite-jekyll .
 ```
- Attendez que la construction soit terminée.

## Étape 3 : Démarrer le Conteneur Docker
- Exécutez la commande 
```
sudo docker run -d -p 4000:4000 --name monsite-jekyll -v $(pwd)/assets:/app/_site/assets monsite-jekyll
```
- Access your web app at `http://localhost:4000`.

## Étape 4 : Test partie modifiable
- **Modification** 
- Grâce au paramètre **-v $(pwd)/assets:/app/_site/assets** vous pouvez modifier le fichier editable.txt et en actualisant le page vous verrez voir les modifications (**F5** ou **ctrl+F5**).
